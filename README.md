Simulação no NS-3 da Aplicação de Controle para Dispositivos Móveis

Topologia da Rede WiFi Ad-hoc
n0      n1      n2........nN
CC      |       |          |
|<----->|<----->|<-------->|
|<------------->|<-------->|
|<------------------------>|

CC - Control Center
nk - Nó k (de 0 a N)
n0 é o nó de controle e se comunica com todos os outros nós
