#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <dirent.h>
#include <list>
#include "GnuplotStream.hpp"

#define VERMELHO "\033[0;31m"
#define NORMAL "\033[0m"
#define RTT "rtt"
#define PDR "pdr"
#define DIRETORY "."

using namespace std;

bool searchFile(string dir);
bool scriptRtt(string dir);
bool scriptPdr(string dir);
bool scriptRttxPdr(string dir);

int main(int argc, char** argv) {
  string diretory;
  if(argc==2){
    diretory=string(argv[1]);
  }else{
    cout << VERMELHO << "Nehum diretório informado, usando diretório padrão: " << DIRETORY << NORMAL << endl;
    diretory=string(DIRETORY);
  }
  if(searchFile(diretory)){
    if(!scriptRtt(diretory)){
      cout << VERMELHO << "Não foi possível criar o arquivo de script." << endl;
      return EXIT_FAILURE;
    }
    if(!scriptPdr(diretory)){
      cout << VERMELHO << "Não foi possível criar o arquivo de script." << endl;
      return EXIT_FAILURE;
    }
  }else{
    cout << VERMELHO << "Arquivo resultados.txt não encontrado no diretório: " << diretory << endl;
    return EXIT_FAILURE;
  }
  try{
    GnuplotStream g;
    g.executarScript(string("rtt.gnu"));
    g.executarScript(string("pdr.gnu"));
  }catch(GnuplotStreamException eg){
      cout << VERMELHO << eg.what() << NORMAL << endl;
      cout << VERMELHO << "Somente os scripts foram criados!!" << NORMAL << endl;
  }
  return EXIT_SUCCESS;
}

bool scriptRtt(string dir){
  fstream file("rtt.gnu",ios::out);
  if (file.is_open()){
    file << "reset" << endl;
    file << "set terminal postscript eps size 20cm,15cm enhanced color solid font 'Helvetica,20' linewidth 2" << endl;
    file << "set output '"<< string(RTT) << ".eps'" << endl;
    file << "unset key" << endl;
    //file << "set key inside right top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000" << endl;
    file << "unset logscale" << endl;
    file << "set style line 1 lw 2 lc rgb 'gray'" << endl;
    file << "set style data histogram" << endl;
    file << "set style histogram errorbars gap 10 lw 1" << endl;
    file << "set bars fullwidth" << endl;
    file << "set mytics 5" << endl;
    file << "set ytics 0.5" << endl;
    file << "set grid mytics" << endl;
    file << "set grid ytics" << endl;
    file << "set grid back ls 1" << endl;
    file << "set boxwidth 5 relative" << endl;
    file << "set style fill solid 1.00 border 0" << endl;
    file << "set title 'Round Trip Time Average (RTT)'" << endl;
    file << "set xlabel 'Nodes Number'" << endl;
    file << "set ylabel 'Round Trip Time(segundos)'" << endl;
    file << "plot '" << dir << "/resultados.txt' u (column(2)):(column(3)):xtic(1) t 'RTT Average' linecolor rgb '#0000FF';" << endl;
    file << "unset output" << endl;
    file.close();
    return true;
  }
  return false;
}

bool scriptPdr(string dir){
  fstream file("pdr.gnu",ios::out);
  if(file.is_open()){
    file << "reset" << endl;
    file << "set terminal postscript eps size 20cm,15cm enhanced color solid font 'Helvetica,20' linewidth 2" << endl;
    file << "set output '"<< string(PDR) << ".eps'" << endl;
    file << "unset key" << endl;
    //file << "set key inside right top vertical Right noreverse enhanced autotitles box linetype -1 linewidth 1.000" << endl;
    file << "unset logscale" << endl;
    file << "set style line 1 lw 2lc rgb 'gray'" << endl;
    file << "set style data histogram" << endl;
    file << "set style histogram errorbars gap 10 lw 1" << endl;
    file << "set bars fullwidth" << endl;
    file << "set mytics 5" << endl;
    file << "set ytics 5" << endl;
    file << "set grid mytics" << endl;
    file << "set grid ytics" << endl;
    file << "set grid ls 1" << endl;
    file << "set boxwidth 5 relative" << endl;
    file << "set style fill solid 1.00 border 0" << endl;
    file << "set title 'Packets Delivery Rate Average (PDR)'" << endl;
    file << "set xlabel 'Nodes Number'" << endl;
    file << "set ylabel 'Packets Delivery Rate(%)'" << endl;
    file << "plot '" << dir << "/resultados.txt' u (column(4)):(column(5)):xtic(1) t 'PDR Average' linecolor rgb '#FF0000';" << endl;
    file << "unset output" << endl;
    file.close();
    return true;
  }
  return false;
}

bool searchFile(string dir){
  DIR * diretorio = opendir(dir.c_str());
  struct dirent *dp;
  if(diretorio!=NULL){
    while ((dp = readdir(diretorio)) != NULL){
      if(string(dp->d_name).compare("resultados.txt")==0){
        (void)closedir(diretorio);
        return true;
      }
    }
  }
  return false;
}