reset
set terminal postscript eps size 20cm,15cm enhanced color solid font 'Helvetica,20' linewidth 2
set output 'pdr.eps'
unset key
unset logscale
set style line 1 lw 2lc rgb 'gray'
set style data histogram
set style histogram errorbars gap 10 lw 1
set bars fullwidth
set mytics 5
set ytics 5
set grid mytics
set grid ytics
set grid ls 1
set boxwidth 5 relative
set style fill solid 1.00 border 0
set title 'Packets Delivery Rate Average (PDR)'
set xlabel 'Nodes Number'
set ylabel 'Packets Delivery Rate(%)'
plot '/mnt/d/welli/Dropbox/Dropbox/Mestrado/RedesSemFio/Trabalho/program/resultados.txt' u (column(4)):(column(5)):xtic(1) t 'PDR Average' linecolor rgb '#FF0000';
unset output
