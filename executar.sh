#!/bin/bash
##Execute on diretory program ./execute.sh R N V where R is the number of simulation  N is the nodes number and V the number of increase the nodes number##
LOCAL=$(pwd)
NS="/home/wellington/repos/ns-3-allinone/ns-3.27/"


if [ $# -eq 5 ]; then
	R=$1 #simulations number
	N=$2 #the initial nodes number
	V=$3 #increase of the nodes number
	C=$4 #round number
	G=$5 #generete graphic or not
	#change to NS-3 directory
	echo "$LOCAL to $NS"
	cd $NS
	#copy the application module to source of NS-3 diretory
	cp -R "$LOCAL/applications" "src/"
	#copy the example of execution application module to example of NS-3 diretory
	cp -R "$LOCAL/examples" "."
	#copy the example file to scratch of NS-3 diretory
	cp -R "examples/udp/request-response.cc" "scratch/request-response.cc"
	#copy the application program to scratch of NS-3 diretory
	cp -R "$LOCAL/simulation" "scratch/"
	#Configure the compilation and execute simulation
	CXXFLAGS="-O3" ./waf configure && ./waf --run="simulation --MaxPackets=3000 --ExecutionMode=1 --Timeout=1.0 --Rounds=$R --Nodes=$N --VariationNodesNumber=$V --Round=$C"
	#for execute example use ./waf --run "request-response --MaxPackets=3000 --ExecutionMode=1 --Timeout=1.0 --Round=$R --Nodes=$N"
	#Return to program deretory
	echo "$NS to $LOCAL"
	cd $LOCAL
	#generate graphs the simulation if pass true
	if [ $G = "true" ] || [ $G = "t" ] || [ $G = "v" ] || [ $G = "verdadeiro" ]; then
		echo "$LOCAL to graphics"
		cd graphics
		g++ graphics.cpp -o graphics
		./graphics $NS
		cd $LOCAL
	fi
else
	#Only generate the simulation graphs
	echo "$LOCAL to graphics"
	cd graphics
	g++ graphics.cpp -o graphics
	./graphics $LOCAL
	cd $LOCAL
fi