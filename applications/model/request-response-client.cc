/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2008 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Wellington Rodrigo de Freitas Costa <wellington@ime.uerj.br>
 */

#include "ns3/log.h"
#include "ns3/ipv4-address.h"
#include "ns3/ipv6-address.h"
#include "ns3/network-module.h"
#include "ns3/ipv4.h"
#include "ns3/nstime.h"
#include "ns3/inet-socket-address.h"
#include "ns3/inet6-socket-address.h"
#include "ns3/socket.h"
#include "ns3/simulator.h"
#include "ns3/socket-factory.h"
#include "ns3/packet.h"
#include "ns3/uinteger.h"
#include "ns3/trace-source-accessor.h"
#include "request-response-client.h"
#include "calculos.h"
#include <cstdlib>
#include <string>
#include <iomanip>
#include <iostream>

namespace ns3 {

  NS_LOG_COMPONENT_DEFINE ("RequestResponseClientApplication");

  NS_OBJECT_ENSURE_REGISTERED (RequestResponseClient);

  TypeId
  RequestResponseClient::GetTypeId (void){
    static TypeId tid = TypeId ("ns3::RequestResponseClient")
      .SetParent<Application> ()
      .SetGroupName("Applications")
      .AddConstructor<RequestResponseClient> ()
      .AddAttribute ("MaxPackets",
                     "The maximum number of packets the application will send",
                       UintegerValue (10),
                       MakeUintegerAccessor (&RequestResponseClient::m_max),
                       MakeUintegerChecker<uint32_t> ())
      .AddAttribute ("Timeout",
                     "The time to wait for send other packet if not received acknowledge (seconds).",
                     TimeValue(Seconds (1.0)),
                     MakeTimeAccessor (&RequestResponseClient::m_interval),
                     MakeTimeChecker ())
      .AddAttribute ("RemoteAddress", 
                     "The destination Address of the outbound packets",
                     AddressValue (),
                     MakeAddressAccessor (&RequestResponseClient::m_peerAddress),
                     MakeAddressChecker ())
      .AddAttribute ("RemotePort", 
                     "The destination port of the outbound packets (1024 to 65535)",
                     UintegerValue (1025),
                     MakeUintegerAccessor (&RequestResponseClient::m_peerPort),
                     MakeUintegerChecker<uint16_t> (1024,65535))
      .AddAttribute ("ExecutionMode", 
                     "Execution mode (0 - by time 1 - by packets received 2 - by samples)",
                     UintegerValue (0),
                     MakeUintegerAccessor (&RequestResponseClient::m_mode),
                     MakeUintegerChecker<uint8_t> (0,2))
      .AddAttribute ("PacketSize", "Message size to generate (0 to 1275bytes).",
                     UintegerValue (1400),
                     MakeUintegerAccessor (&RequestResponseClient::m_dataSize),
                     MakeUintegerChecker<uint16_t> (1,1400))
      .AddAttribute ("IntervalTrust", "Interval of trust of the simulation.",
                     UintegerValue (95),
                     MakeUintegerAccessor (&RequestResponseClient::m_trust),
                     MakeUintegerChecker<uint16_t> (70,100))
      .AddTraceSource ("Tx", "A new packet is created and is sent",
                       MakeTraceSourceAccessor (&RequestResponseClient::m_txTrace),
                       "ns3::Packet::TracedCallback")
    ;
    return tid;
  }

  RequestResponseClient::RequestResponseClient (){
    NS_LOG_FUNCTION (this);
    m_sent      = 0;
    m_received  = 0;
    m_socket    = 0;
    m_sendEvent = EventId ();
    m_trust=m_calc.setZ(m_trust);
    m_error=1.0-(m_trust/100);
    if(m_dataSize<1 || m_dataSize >1400)m_dataSize=1400;    
    m_data      = new uint8_t [m_dataSize];
    for (uint16_t i = 0; i < m_dataSize; ++i){
      if(i % 2){
          m_data[i] = (uint8_t) ((i%(26))+65);
      }else{
          m_data[i] = (uint8_t) ((i%(26))+97);
      }
    }
  }

  RequestResponseClient::~RequestResponseClient(){
    NS_LOG_FUNCTION (this);
    m_socket = 0;
    delete [] m_data;
    m_data = 0;
    m_dataSize = 0;
  }

  void 
  RequestResponseClient::SetRemote (Address ip, uint16_t port){
    NS_LOG_FUNCTION (this << ip << port);
    m_peerAddress = ip;
    m_peerPort = port;
  }

  void 
  RequestResponseClient::SetRemote (Address addr){
    NS_LOG_FUNCTION (this << addr);
    m_peerAddress = addr;
  }

  void
  RequestResponseClient::DoDispose (void){
    NS_LOG_FUNCTION (this);
    Application::DoDispose ();
  }

  void 
  RequestResponseClient::StartApplication (void){
    NS_LOG_FUNCTION (this);

    if (m_socket == 0)    {
      TypeId tid = TypeId::LookupByName ("ns3::UdpSocketFactory");
      m_socket = Socket::CreateSocket (GetNode (), tid);
      if (Ipv4Address::IsMatchingType(m_peerAddress) == true)        {
          if (m_socket->Bind () == -1)            {
            NS_FATAL_ERROR(VERMELHO << "Failed to bind socket");
          }
          m_socket->Connect (InetSocketAddress (Ipv4Address::ConvertFrom(m_peerAddress), m_peerPort));
      }else
        if (Ipv6Address::IsMatchingType(m_peerAddress) == true){
          if (m_socket->Bind6 () == -1){
            NS_FATAL_ERROR(VERMELHO << "Failed to bind socket");
          }
          m_socket->Connect (Inet6SocketAddress (Ipv6Address::ConvertFrom(m_peerAddress), m_peerPort));
        }else 
          if (InetSocketAddress::IsMatchingType (m_peerAddress) == true){
            if (m_socket->Bind () == -1){
              NS_FATAL_ERROR(VERMELHO << "Failed to bind socket");
            }
            m_socket->Connect (m_peerAddress);
          }else
            if (Inet6SocketAddress::IsMatchingType (m_peerAddress) == true){
              if (m_socket->Bind6 () == -1){
                NS_FATAL_ERROR(VERMELHO << "Failed to bind socket");
              }
              m_socket->Connect (m_peerAddress);
            }else{
              NS_ASSERT_MSG (false,VERMELHO << "Incompatible address type: " << m_peerAddress);
            }
      m_id = (this->GetNode()->GetObject<Ipv4>()->GetAddress(1,0).GetLocal().Get() & 0x000000FF)-1;
    }

    m_socket->SetRecvCallback (MakeCallback (&RequestResponseClient::HandleRead, this));
    m_socket->SetAllowBroadcast (true);
    ScheduleTransmit (Seconds (0.0),true);
    std::cout << VERDE << "Client "<< m_id << " Started" << std::endl;
  }

  void 
  RequestResponseClient::StopApplication (){
    NS_LOG_FUNCTION (this);

    if (m_socket != 0){
      m_socket->Close ();
      m_socket->SetRecvCallback (MakeNullCallback<void, Ptr<Socket> > ());
      m_socket = 0;
    }
    Simulator::Cancel (m_sendEvent);
    std::cout << VERDE << "Client "<< m_id << " Finished" << std::endl;
  }


  void 
  RequestResponseClient::SetDataSize (uint32_t dataSize){
    NS_LOG_FUNCTION (this << dataSize);

    if((dataSize!=m_dataSize) && (dataSize > 0) && (dataSize <= 1400)){
      m_dataSize=dataSize;
      delete [] m_data;
      m_data = new uint8_t [m_dataSize];
      for (uint32_t i = 0; i < m_dataSize; ++i){
        if(i % 2){
            m_data[i] = (uint8_t) ((i%(26))+65);
        }else{
            m_data[i] = (uint8_t) ((i%(26))+97);
        }
      }
    }
  }

  uint32_t 
  RequestResponseClient::GetDataSize (void) const{
    NS_LOG_FUNCTION (this);
    return m_dataSize;
  }

  void 
  RequestResponseClient::SetFill (std::string fill){
    NS_LOG_FUNCTION (this << fill);

    if((fill.length() > 0) && (fill.length() <=1400)){
      m_dataSize=fill.length();
      delete [] m_data;
      m_data = new uint8_t [m_dataSize];
      for (uint32_t i = 0; i < m_dataSize; ++i){
        m_data[i] = fill[i];
      }
    }
  }

  void 
  RequestResponseClient::SetFill (uint8_t fill, uint32_t dataSize){
    NS_LOG_FUNCTION (this << fill << dataSize);
    
    if((dataSize > 0) && (dataSize <= 1400)){
      m_dataSize=dataSize;
      delete [] m_data;
      m_data = new uint8_t [m_dataSize];
      for (uint32_t i = 0; i < m_dataSize; ++i){
        m_data[i] = fill;
      }
    }
  }

  void
  RequestResponseClient::SetFill (uint8_t *fill, uint32_t fillSize, uint32_t dataSize){
    NS_LOG_FUNCTION (this << fill << fillSize << dataSize);
    
   if((dataSize > 0) && (dataSize <= 1400) && (fillSize == dataSize)){
      m_dataSize=dataSize;
      delete [] m_data;
      m_data = new uint8_t [m_dataSize];
      for (uint32_t i = 0; i < m_dataSize; ++i){
        m_data[i] = fill[i];
      }
    }
  }

  void 
  RequestResponseClient::SetFill (uint8_t *fill, uint32_t fillSize){
    if ((fillSize <= 1400) && (fillSize > 0)){
      m_dataSize=fillSize;
      delete [] m_data;
      m_data = new uint8_t [m_dataSize];
      for (uint32_t i = 0; i < m_dataSize; ++i){
        m_data[i] = fill[i];
      }
    }
  }

  void 
  RequestResponseClient::ScheduleTransmit (Time dt,bool first){
    NS_LOG_FUNCTION (this << dt);
    m_sendEvent = Simulator::Schedule (dt, &RequestResponseClient::Send, this,first);  
  }

  void 
  RequestResponseClient::Send (bool first){
    NS_LOG_FUNCTION (this);

    NS_ASSERT (m_sendEvent.IsExpired ());

    Ptr<Packet> p;
    /*call to the trace sinks before the packet is actually sent,
    so that tags added to the packet can be sent as well*/
    if((m_dataSize > 0) && (m_data != 0)){
      NS_ASSERT_MSG (m_dataSize <= 1400, "RequestResponseClient::Send(): m_dataSize is greater 1400bytes");
      //NS_ASSERT_MSG (m_data, "RequestResponseClient::Send(): m_dataSize but no m_data");
    }else{
        m_dataSize=1400;
        m_data = new uint8_t [m_dataSize];
        for (uint32_t i = 0; i < m_dataSize; ++i){
          if(i % 2){
              m_data[i] = (uint8_t) ((i%(26))+65);
          }else{
              m_data[i] = (uint8_t) ((i%(26))+97);
          }
        }
    }
    p = Create<Packet> (m_data, m_dataSize);
    m_txTrace (p);
    if(m_socket->Send (p)>=0){
      if(first){
        m_timeSent=Simulator::Now ().GetSeconds ();
      }
      ++m_sent;
      if (Ipv4Address::IsMatchingType (m_peerAddress)){
        NS_LOG_INFO (VERDE <<
                     "Client:"              << m_id <<
                     "At time "             << m_timeSent <<
                     "s client sent the "   << m_sent <<
                     " packet number with " << p->GetSize() <<
                     " bytes to "           << Ipv4Address::ConvertFrom (m_peerAddress) <<
                     " port "               << m_peerPort);
      }else if (Ipv6Address::IsMatchingType (m_peerAddress)){
          NS_LOG_INFO (VERDE <<
                       "Client:"              << m_id <<
                       "At time "             << m_timeSent <<
                       "s client sent the "   << m_sent <<
                       " packet number with " << p->GetSize() <<
                       " bytes to "           << Ipv6Address::ConvertFrom (m_peerAddress) <<
                       " port "               << m_peerPort);
        }else if (InetSocketAddress::IsMatchingType (m_peerAddress)){
            NS_LOG_INFO (VERDE <<
                         "Client:"              << m_id <<
                         "At time "             << m_timeSent <<
                         "s client sent the "   << m_sent <<
                         " packet number with " << p->GetSize() <<
                         " bytes to "           << InetSocketAddress::ConvertFrom (m_peerAddress).GetIpv4 () <<
                         " port "               << InetSocketAddress::ConvertFrom (m_peerAddress).GetPort ());
          }else if (Inet6SocketAddress::IsMatchingType (m_peerAddress)){
              NS_LOG_INFO (VERDE <<
                           "Client:"              << m_id <<
                           "At time "             << m_timeSent <<
                           "s client sent the "   << m_sent <<
                           " packet number with " << p->GetSize() <<
                           " bytes to "           << Inet6SocketAddress::ConvertFrom (m_peerAddress).GetIpv6 () <<
                           " port "               << Inet6SocketAddress::ConvertFrom (m_peerAddress).GetPort ());
            }
      ScheduleTransmit (m_interval,false);
    }else{
      ScheduleTransmit (Seconds (0.0),first);
    }
  }

  void
  RequestResponseClient::HandleRead (Ptr<Socket> socket){
    NS_LOG_FUNCTION (this << socket);
    Ptr<Packet> packet;
    Address from;
    Address local;
    m_socket->GetSockName(local);
    InetSocketAddress localAddress = InetSocketAddress::ConvertFrom (local);
    while ((packet = socket->RecvFrom (from))){
      Simulator::Cancel (m_sendEvent);
      m_timeReceived=Simulator::Now ().GetSeconds ();
      if (packet->GetSize() == m_dataSize){
        if(m_timeReceived-m_timeSent > 0){
          m_calc.pushValor(m_timeReceived-m_timeSent);
          ++m_received;
          if (InetSocketAddress::IsMatchingType (from)){
            NS_LOG_INFO (VERDE <<
                         "Client:"                << m_id <<
                         "At time "               << m_timeReceived <<
                         "s client received the " << m_received <<
                         " packet number with "   << packet->GetSize () <<
                         " bytes from "           << InetSocketAddress::ConvertFrom (from).GetIpv4 () <<
                         " port "                 << InetSocketAddress::ConvertFrom (from).GetPort ());
          }else if (Inet6SocketAddress::IsMatchingType (from)){
              NS_LOG_INFO (VERDE <<
                           "Client:"                << m_id <<
                           "At time "               << m_timeReceived <<
                           "s client received the " << m_received <<
                           " packet number with "   << packet->GetSize () <<
                           " bytes from "           << Inet6SocketAddress::ConvertFrom (from).GetIpv6 ()  <<
                           " port "                 << Inet6SocketAddress::ConvertFrom (from).GetPort ());
            }
        }else{
          --m_sent;//discart if time is invalid and send new datagram and restart time
        }
      }
      if((m_received % (uint32_t)(m_max/10))==0){
        std::cout << VERDE << std::fixed << std::setprecision(6);
        std::cout << "Client:"         << m_id
                  << "\tSamples: "     << getRttSamples()
                  << "\tRTT Average: " << getRttAverage()
                  << "s\tRTT Error: "  << (uint32_t)(getRttError()*100)
                  << "%\tPDR: "        << (uint32_t)(getPdr()*100) << "%" << std::endl;
      }
      switch(m_mode){
        case 2:
          if(m_received >= m_max){
            if((m_received >= getRttN()) && (m_error >= getRttError())){
              StopApplication();
              break;  
            }
          }
          ScheduleTransmit (Seconds (0.0),true);
          break;
        case 1:
          if(m_received >= m_max){
            StopApplication();
            break;
          }
          ScheduleTransmit (Seconds (0.0),true);
          break;
        default:
          ScheduleTransmit (Seconds (0.0),true);
      };
    }
  }
} // Namespace ns3
